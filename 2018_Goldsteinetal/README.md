## Literature-based latitudinal distribution and possible range shifts of two US east coast dune grass species (*Uniola paniculata* and *Ammophila breviligulata*


**Evan B. Goldstein, Elsemarie Mullins, Laura J. Moore, Reuben G. Biel, Joseph K. Brown, Sally D. Hacker, Katya R. Jay, Rebecca S Mostow, Peter Ruggiero, Julie K. Zinnert**


#### Paper
Manuscript accepted.


#### Citation
EB Goldstein, E Mullins, LJ Moore, RG Biel, JK Brown, SD Hacker, KR Jay, RS Mostow, P Ruggiero, JK Zinnert. (accepted). Literature-based latitudinal distribution and possible range shifts of two US east coast dune grass species (*Uniola paniculata* and *Ammophila breviligulata*), PeerJ.